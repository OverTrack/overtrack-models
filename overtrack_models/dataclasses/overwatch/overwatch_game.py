import datetime
from typing import Optional, Tuple, Dict

from dataclasses import dataclass, field

from overtrack_models.dataclasses.overwatch.basic_types import GameResult, Map, Mode, Role, Season
from overtrack_models.dataclasses.overwatch.killfeed import Killfeed
from overtrack_models.dataclasses.overwatch.performance_stats import PerformanceStats
from overtrack_models.dataclasses.overwatch.stages import Stages
from overtrack_models.dataclasses.overwatch.teamfights import Teamfights
from overtrack_models.dataclasses.overwatch.teams import PlayerRank, Teams


@dataclass
class OverwatchGame:
    key: str
    timestamp: float
    duration: float
    season: Season

    map: Map
    attacking: Optional[bool] = field(metadata={'fallback': None, 'added_in': '2.2.0'})
    rounds: Optional[int] = field(metadata={'fallback': None, 'added_in': '2.3.0'})

    start_sr: Optional[int]
    end_sr: Optional[int]
    placement: Optional[bool]

    mode: Mode
    role: Optional[Role]
    competitive: bool
    custom_game: bool
    teams_fixed: bool
    replay: bool
    valid: bool

    rank: Optional[PlayerRank]
    stages: Stages
    teams: Teams
    killfeed: Killfeed
    teamfights: Optional[Teamfights] = field(metadata={'fallback': None})
    stats: PerformanceStats

    final_score: Optional[Tuple[int, int]]
    result: GameResult

    frame_count: int
    images: Dict[str, str] = field(metadata={'fallback_factory': dict, 'added_in': '2.3.0'})
    log_uri: Optional[str] = field(metadata={'fallback': None, 'added_in': '2.6.2'})

    version: str = '2.0.0'

    @property
    def time(self) -> datetime.datetime:
        return datetime.datetime.fromtimestamp(self.timestamp)
