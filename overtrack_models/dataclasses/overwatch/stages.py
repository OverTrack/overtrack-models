from typing import List, Optional, Tuple, Iterator

from dataclasses import dataclass, field

from overtrack_models.dataclasses.util import Literal, s2ts


@dataclass
class OvertimePeriod:
    start: float
    stage_relative_start: float
    end: float

    @property
    def duration(self) -> float:
        return self.end - self.start

    def __repr__(self) -> str:
        return f'OvertimePeriod(' \
               f'{s2ts(self.stage_relative_start, zpad=False)} > {s2ts(self.stage_relative_start + self.duration, zpad=False)} ' \
               f'(duration={s2ts(self.duration, zpad=False)})' \
               f')'

    __str__ = __repr__


@dataclass
class ControlOwnershipPeriod:
    start: float
    stage_relative_start: float
    end: float

    owner_blue: bool

    @property
    def duration(self) -> float:
        return self.end - self.start

    @property
    def owner(self) -> str:
        return ['red', 'blue'][self.owner_blue]

    def __repr__(self) -> str:
        return f'ControlOwnershipPeriod(' \
               f'{s2ts(self.stage_relative_start, zpad=False)} > {s2ts(self.stage_relative_start + self.duration, zpad=False)} ' \
               f'(duration={s2ts(self.duration, zpad=False)}), ' \
               f'owner={self.owner}' \
               f')'

    __str__ = __repr__


@dataclass
class CheckpointCaptured:
    stage: int
    blue_capture: bool
    timestamp: float
    stage_relative_timestamp: float
    checkpoint_index: int
    score: int

    @property
    def checkpoint(self) -> str:
        if self.checkpoint_index > 3:
            return '?'
        return 'ABC'[self.checkpoint_index - 1]

    def __repr__(self) -> str:
        return f'CheckpointCaptured(' \
               f'{s2ts(self.timestamp, zpad=False)}, ' \
               f'stage {self.stage} +{s2ts(self.stage_relative_timestamp, zpad=False)}, ' \
               f'checkpoint={self.checkpoint_index} ({self.checkpoint}), ' \
               f'score={self.score}' \
               f')'

    __str__ = __repr__


@dataclass
class TickSecured:
    timestamp: float
    tick: int
    checkpoint_index: int

    @property
    def checkpoint(self) -> str:
        return 'ABC'[self.checkpoint_index - 1]

    def __repr__(self) -> str:
        return f'TickSecured(' \
               f'{s2ts(self.timestamp, zpad=False)}, ' \
               f'tick={self.tick}, ' \
               f'checkpoint={self.checkpoint_index} ({self.checkpoint})' \
               f')'

    __str__ = __repr__


@dataclass
class PayloadMovingPeriod:
    start: float
    end: float
    stage_relative_start: float
    forwards: bool

    @property
    def duration(self) -> float:
        return self.end - self.start

    @property
    def direction(self) -> Literal['forwards', 'backwards']:
        r: Literal['forwards', 'backwards']
        if self.forwards:
            r = 'forwards'
        else:
            r = 'backwards'
        return r

    def __repr__(self) -> str:
        return f'PayloadMovingPeriod(' \
               f'{s2ts(self.stage_relative_start, zpad=False)} > {s2ts(self.stage_relative_start + self.duration, zpad=False)} ' \
               f'(duration={s2ts(self.duration, zpad=False)}), ' \
               f'direction={self.direction}' \
               f')'

    __str__ = __repr__


@dataclass
class Stage:
    index: int
    name: str

    # TODO: prepare_start: float
    start: float
    absolute_start: float
    end: float
    absolute_end: float

    overtime_periods: List[OvertimePeriod]

    @property
    def duration(self) -> float:
        return self.end - self.start


@dataclass
class ControlStage(Stage):
    winner_blue: Optional[bool] = None
    end_score: Optional[Tuple[int, int]] = None

    ownership_periods: List[ControlOwnershipPeriod] = field(default_factory=list)


@dataclass
class CheckpointStage(Stage):
    attacking: bool

    start_score: Optional[int] = None
    end_score: Optional[int] = None

    checkpoints_captured: List[CheckpointCaptured] = field(default_factory=list)


@dataclass
class AssaultStage(CheckpointStage):
    ticks: List[object] = field(default_factory=list)


@dataclass
class EscortStage(CheckpointStage):
    payload_moving_periods: List[PayloadMovingPeriod] = field(default_factory=list)


@dataclass
class HybridStage(AssaultStage, EscortStage):
    payload_moving_periods: List[PayloadMovingPeriod] = field(default_factory=list)


@dataclass
class Stages:
    stages: List[Stage]
    time_in_stages: float

    def __iter__(self) -> Iterator[Stage]:
        return iter(self.stages)

    def __getitem__(self, i: int) -> Stage:
        return self.stages[i]

    def __len__(self) -> int:
        return len(self.stages)

    def get_stage_at(self, timestamp: float) -> Optional[Stage]:
        for s in self.stages:
            if s.start <= self.stages[0].start + timestamp <= s.end:
                return s
        else:
            return None
