from dataclasses import field, dataclass
from typing import List, Optional, TYPE_CHECKING, Iterator

from overtrack_models.dataclasses.util import s2ts

if TYPE_CHECKING:
    from overtrack_models.dataclasses.overwatch.teams import Player


@dataclass
class KillfeedPlayer:
    hero: str
    name: str
    name_match: Optional[float] = None
    blue_team: Optional[bool] = None

    index: Optional[int] = None


@dataclass
class KillRow:
    left: Optional[KillfeedPlayer]
    right: KillfeedPlayer
    y: int
    resurrect: Optional[bool] = False
    headshot: bool = False
    assists: List[str] = field(default_factory=list)
    ability: Optional[str] = None
    from_killcam: Optional[bool] = None


_Parent = Player if TYPE_CHECKING else object  # type: Player

@dataclass
class KillfeedPlayer(_Parent):
    player: 'Player' = field(default_factory=lambda: None)
    hero: str = field(default_factory=lambda: None)

    def __getattr__(self, attr: str):
        return getattr(object.__getattribute__(self, 'player'), attr)


@dataclass
class EliminationAssist:
    player: 'Player'
    hero: Optional[str]
    percentage: Optional[int]
    kill: Optional['Kill'] = None

    @property
    def timestamp(self) -> float:
        return self.kill.timestamp

    def __repr__(self) -> str:
        return (
            f'KillfeedAssist('
            f'player={self.player.name!r}, '
            f'hero={self.hero}, '
            f'percentage={self.percentage}, '
            f'kill={self.kill}'
            f')'
        )

    __str__ = __repr__


@dataclass
class KillfeedAssist:
    player: Optional['Player']
    hero: str
    kill: Optional['Kill'] = None

    @property
    def timestamp(self) -> float:
        return self.kill.timestamp

    def __repr__(self) -> str:
        return (
            f'KillfeedAssist('
            f'player={self.player.name if self.player else None!r}, '
            f'hero={self.hero}, '
            f'kill={self.kill}'
            f')'
        )

    __str__ = __repr__


@dataclass
class Kill:
    timestamp: float
    index: int
    rows: List[KillRow]
    row_timestamps: List[float]

    left: Optional[KillfeedPlayer] = field(metadata={
        'migration': lambda data: {'player': data['left'], 'hero': data['left_hero']} if data.get('left_hero') else data.get('left'),
        'added_in': '2.4.0',
    })
    right: KillfeedPlayer = field(metadata={
        'migration': lambda data: {'player': data['right'], 'hero': data['right_hero']} if 'right_hero' in data else data['right'],
        'added_in': '2.4.0',
    })

    elimination_assists: List[EliminationAssist] = field(default_factory=list)
    killfeed_assists: List[KillfeedAssist] = field(default_factory=list)
    ability: Optional[str] = None

    @property
    def suicide(self) -> bool:
        return self.left is None

    @property
    def resurrect(self) -> bool:
        return self.left is not None and self.left.blue_team == self.right.blue_team

    def __str__(self) -> str:
        s = f'Kill({s2ts(self.timestamp)}, {self.index}'
        if not self.suicide:
            s += f', left={["red", "blue"][self.left.blue_team]} {self.left.hero} {self.left.name!r}'
        s += f', right={["red", "blue"][self.right.blue_team]} {self.right.hero} {self.right.name!r}'
        if self.ability:
            s += f', ability={self.ability}'
        if self.killfeed_assists:
            s += f', killfeed_assists=[' + ', '.join([f'{a.hero} {a.player.name if a.player else None!r}' for a in self.killfeed_assists]) + ']'
        if self.elimination_assists:
            s += f', elimination_assists=[' + ', '.join([f'{a.hero} {a.player.name!r}' for a in self.elimination_assists]) + ']'
        s += f', rows={len(self.rows)})'
        return s

    __repr__ = __str__

@dataclass
class Killfeed:
    kills: List[Kill]
    unresolved_kills: int

    def __iter__(self) -> Iterator[Kill]:
        return iter(self.kills)

    def __getitem__(self, i: int) -> Kill:
        return self.kills[i]

    def __len__(self) -> int:
        return len(self.kills)
