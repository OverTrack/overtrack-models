from typing import List, Iterator, Optional, Tuple, Dict

from dataclasses import dataclass, field

from overtrack_models.dataclasses.overwatch.killfeed import Kill, EliminationAssist, KillfeedAssist
from overtrack_models.dataclasses.overwatch.teams import Player, UltHeldPeriod


@dataclass
class Teamfight:
    eliminations: List[Kill]
    elimination_assists: List[EliminationAssist]
    killfeed_assists: List[KillfeedAssist]
    suicides: List[Kill]
    resurrects: List[Kill]
    stage: int

    kills_blue_red: Tuple[int, int]
    blue_winner: Optional[bool]

    staggers: List[Kill]
    missing_at_start: List[Player]

    ults_used: List[UltHeldPeriod]

    composition: Tuple[List[Optional[str]], List[Optional[str]]]
    composition_known: bool

    @property
    def first_blood(self) -> Kill:
        return self.eliminations[0]

    @property
    def timestamp(self) -> float:
        return self.eliminations[0].timestamp

    @property
    def start(self) -> float:
        return self.timestamp

    @property
    def end(self) -> float:
        return self.eliminations[-1].timestamp

    def __iter__(self) -> Iterator[Kill]:
        return iter(self.eliminations)

    def __getitem__(self, i: int) -> Kill:
        return self.eliminations[i]

    def __len__(self) -> int:
        return len(self.eliminations)

    def __lt__(self, other):
        return self.timestamp < other.timestamp


@dataclass
class TeamfightStat:
    during_fights: int
    outside_fights: int

    @property
    def total(self) -> int:
        return self.during_fights + self.outside_fights


@dataclass
class PlayerStats:
    playtime: float
    teamfights: int

    won_fights: int

    eliminations: TeamfightStat
    deaths: TeamfightStat
    suicides: TeamfightStat
    resurrects: Optional[TeamfightStat]

    first_kills: int
    first_deaths: int
    times_staggered: int
    fight_starts_missed: int
    first_kill_fights_won: Optional[int] = field(metadata={
        'added_in': '2.6.0',
        'fallback': None,
    })

    killfeed_assists: TeamfightStat
    elimination_assists: Optional[TeamfightStat]

    ults: Optional[TeamfightStat]


Composition = Tuple[str, str, str, str, str, str]
CompositionStats = Tuple[Composition, PlayerStats]

ALL_STAGES = 'All Stages'
ATTACK_STAGES = 'Attack Stages'
DEFEND_STAGES = 'Defend Stages'


@dataclass
class StageStats:
    blue_stats: PlayerStats
    blue_compositions: List[CompositionStats]

    red_stats: PlayerStats
    red_compositions: List[CompositionStats]

    @property
    def duration(self) -> float:
        return self.blue_stats.playtime


@dataclass
class Teamfights:
    teamfights: List[Teamfight]
    stage_stats: Dict[str, StageStats] = field(
        metadata={
            'added_in': '2.5.0',
            'migration': lambda d: d['stage_stats'] if 'stage_stats' in d else {
                ALL_STAGES: {
                    'blue_stats': d['team_stats'][0],
                    'red_stats': d['team_stats'][1],

                    'blue_compositions': [],
                    'red_compositions': [],
                }
            }
        }
    )

    eliminations_outside_fights: List[Kill]
    suicides_outside_fights: List[Kill]
    resurrects_outside_fights: List[Kill]

    killfeed_assists_outside_fights: List[KillfeedAssist]
    elimination_assists_outside_fights: List[EliminationAssist]

    @property
    def blue_stats(self) -> PlayerStats:
        return self.stage_stats[ALL_STAGES].blue_stats

    @property
    def red_stats(self) -> PlayerStats:
        return self.stage_stats[ALL_STAGES].red_stats

    @property
    def team_stats(self) -> Tuple[PlayerStats, PlayerStats]:
        return self.blue_stats, self.red_stats

    @property
    def eliminations_during_fights(self) -> List[Kill]:
        r: List[Kill] = []
        for tf in self:
            r += tf.eliminations
        return r

    @property
    def suicides_during_fights(self) -> List[Kill]:
        r: List[Kill] = []
        for tf in self:
            r += tf.suicides
        return r

    @property
    def resurrects_during_fights(self) -> List[Kill]:
        r: List[Kill] = []
        for tf in self:
            r += tf.resurrects
        return r

    @property
    def first_bloods(self) -> List[Kill]:
        r: List[Kill] = []
        for tf in self:
            r.append(tf.first_blood)
        return r

    @property
    def staggers(self) -> List[Kill]:
        r: List[Kill] = []
        for tf in self:
            r += tf.staggers
        return r

    @property
    def killfeed_assists_during_fights(self) -> List[KillfeedAssist]:
        r: List[KillfeedAssist] = []
        for tf in self:
            r += tf.killfeed_assists
        return r

    @property
    def elimination_assists_during_fights(self) -> List[EliminationAssist]:
        r: List[EliminationAssist] = []
        for tf in self:
            r += tf.elimination_assists
        return r

    @property
    def ults_during_fights(self) -> List[UltHeldPeriod]:
        r: List[UltHeldPeriod] = []
        for tf in self:
            r += tf.ults_used
        return r

    def __iter__(self) -> Iterator[Teamfight]:
        return iter(self.teamfights)

    def __getitem__(self, i: int) -> Teamfight:
        return self.teamfights[i]

    def __len__(self) -> int:
        return len(self.teamfights)
