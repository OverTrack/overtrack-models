from typing import Dict, List, Optional

from dataclasses import dataclass

from overtrack_models.dataclasses.overwatch.teams import HeroPlayedSegment


@dataclass
class HeroStats:
    hero: Optional[str]
    eliminations: Optional[int]
    objective_kills: Optional[int]
    objective_time: Optional[int]
    hero_damage_done: Optional[int]
    healing_done: Optional[int]
    deaths: Optional[int]

    final_blows: Optional[int]

    hero_specific_stats: Optional[Dict[str, Optional[int]]]

    from_endgame: bool
    time_played: float

    age: int = 0
    warn_age: bool = False
    warn_startend: bool = False

    @property
    def is_all_heroes(self) -> bool:
        return self.hero == 'all heroes'


@dataclass
class PerformanceStats:
    stats: Dict[str, HeroStats]
    heroes_played: Optional[List[HeroPlayedSegment]]
    hero_durations: Optional[Dict[str, float]]

    @property
    def time_played(self) -> float:
        return sum(d.duration for d in self.heroes_played)
