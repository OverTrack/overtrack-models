import copy

import datetime
import itertools
from dataclasses import dataclass, field
from overtrack_models.dataclasses.typedload import referenced_typedload

from overtrack_models.dataclasses.util import s2ts, Literal
from typing import Optional, List, Tuple, Dict

MapName = str
AgentName = str
GameModeName = Literal['unrated', 'competitive', 'custom', 'spike rush']
ClipType = Literal['multikill']
WinType = Literal['elimination', 'spike', 'timer']


@dataclass
class Kill:
    round: int
    index: int
    round_timestamp: float
    timestamp: float

    killer: 'Player'
    killed: 'Player'
    weapon: Optional[str]

    wallbang: bool = False
    headshot: bool = False

    def __str__(self):
        return (
            f'Kill('
                f'round={self.round}, '
                f'{s2ts(self.round_timestamp)}, '
                f'{["Enemy", "Ally"][self.killer.friendly]} {self.killer.agent} {self.killer.name!r} > '
                f'{self.weapon} {"- " if self.wallbang else ""}{"* " if self.headshot else ""}> '
                f'{self.killed.agent} {self.killed.name!r}'
            f')'
        )
    __repr__ = __str__


@dataclass
class Kills:
    kills: List[Kill]

    def __iter__(self):
        return iter(self.kills)
    def __len__(self):
        return len(self.kills)
    def __getitem__(self, item):
        return self.kills[item]


@dataclass
class Ult:
    player: 'Player'
    index: int

    gained: float
    lost: float
    used: bool

    round_gained: int
    round_gained_timestamp: float

    round_lost: int
    round_lost_timestamp: float

    @property
    def held(self) -> float:
        return self.used - self.gained


@dataclass
class Round:
    index: int

    buy_phase_start: float
    start: float
    end: float

    attacking: bool
    won: Optional[bool]
    spike_planted: Optional[float] = field(metadata={'fallback': None})
    spike_planter: Optional['Player'] = field(metadata={'fallback': None})
    win_type: Optional[WinType] = field(metadata={'fallback': None})
    kills: Kills
    ults_used: List[Ult] = field(default_factory=list)


@dataclass
class Rounds:
    rounds: List[Round]
    final_score: Optional[Tuple[int, int]]

    attacking_first: bool
    attack_wins: int
    defence_wins: int

    has_game_resets: bool = False

    def __iter__(self):
        return iter(self.rounds)

    def __len__(self):
        return len(self.rounds)

    def __getitem__(self, item):
        return self.rounds[item]


@dataclass
class PlayerStats:
    avg_combat_score: Optional[int]
    kills: Optional[int]
    deaths: Optional[int]
    assists: Optional[int]
    econ_rating: Optional[int]
    first_bloods: Optional[int]
    plants: Optional[int]
    defuses: Optional[int]


@dataclass
class Stat:
    total: int
    per_round: float
    of_total: Optional[float]


@dataclass
class WinCorrelatedStat:
    total: int
    per_round: float
    of_total: Optional[float]

    led_to_win: int
    led_to_win_ratio: Optional[float]

    led_to_loss: int
    led_to_loss_ratio: Optional[float]


@dataclass
class PerformanceStats:
    kills: Stat
    deaths: Stat
    kills_per_death: Optional[float]

    team_firstbloods: WinCorrelatedStat
    team_firstdeaths: WinCorrelatedStat

    firstbloods: WinCorrelatedStat
    firstdeaths: WinCorrelatedStat


@dataclass
class Player:
    agent: Optional[AgentName]
    name: Optional[str]
    friendly: bool

    ults: List[Ult] = field(default_factory=list)
    stats: Optional[PlayerStats] = field(repr=False, default=None)

    kills: List[Kill] = field(repr=False, default_factory=list)
    deaths: List[Kill] = field(repr=False, default_factory=list)
    weaponkills: Dict[Optional[str], List[Kill]] = field(repr=False, default_factory=dict)

    performance: Optional[PerformanceStats] = None


@dataclass
class Team:
    players: List[Player]
    friendly: bool

    performance: Optional[PerformanceStats] = None

    def __iter__(self):
        return iter(self.players)
    def __len__(self):
        return len(self.players)
    def __getitem__(self, item):
        return self.players[item]


@dataclass
class Teams:
    team1: Team
    team2: Team

    firstperson: Optional[Player]
    have_scoreboard: bool

    @property
    def teams(self) -> Tuple[Team, Team]:
        return self.team1, self.team2
    @property
    def players(self) -> List[Player]:
        return self.team1.players + self.team2.players
    def __iter__(self):
        return iter(self.teams)
    def __len__(self):
        return len(self.teams)
    def __getitem__(self, item):
        return self.teams[item]


@dataclass
class Clip:
    url: Optional[str]
    title: str

    type: ClipType
    metadata: Dict


@dataclass
class ValorantGame:
    key: str
    timestamp: float
    duration: float

    spectated: bool
    rank: Optional[str] = field(metadata={'fallback': None})

    won: Optional[bool]

    map: MapName
    game_mode: GameModeName
    rounds: Rounds
    teams: Teams

    start_pts: Optional[float]
    vod: Optional[str]
    clips: Optional[List[Clip]]

    season_mode_id: int
    frames_count: int
    game_version: Optional[str]

    version: str

    @property
    def score(self) -> Tuple[int, int]:
        return self.rounds.final_score

    @property
    def time(self) -> datetime.datetime:
        return datetime.datetime.fromtimestamp(self.timestamp)

    @property
    def kills(self) -> List[Kill]:
        return list(itertools.chain(*[r.kills for r in self.rounds]))

    @classmethod
    def from_dict(cls, data: Dict) -> 'ValorantGame':
        to_load = copy.deepcopy(data)

        if 'game_version' not in to_load:
            to_load['game_version'] = None

        idgen = itertools.count()
        def ref(d: Dict):
            if '_id' not in d:
                d['_id'] = next(idgen)
            return {'_ref': d['_id']}

        players = {}
        for t in [to_load['teams'][f'team{i + 1}'] for i in range(2)]:
            for p in t['players']:
                players[(p['friendly'], p['agent'])] = p
                for u in p.get('ults', []):
                    u['player'] = ref(p)

        if to_load['teams']['firstperson']:
            to_load['teams']['firstperson'] = ref(players[(True, to_load['teams']['firstperson'])])

        kills = {}
        for r in to_load['rounds']['rounds']:
            for k in r['kills'].get('kills', []):
                kills[(r['index'], k['index'])] = k
                k['killer'] = ref(players[tuple(k['killer'])])
                k['killed'] = ref(players[tuple(k['killed'])])

            if 'ults_used' in r:
                r['ults_used'] = [
                    ref(players[tuple(u[0])]['ults'][u[1]]) for u in r['ults_used']
                ]

            if r.get('spike_planter'):
                r['spike_planter'] = ref(players[tuple(r['spike_planter'])])

        lists_of_kills = []
        for p in players.values():
            lists_of_kills.append(p.get('kills', []))
            lists_of_kills.append(p.get('deaths', []))
            lists_of_kills += list(p.get('weaponkills', {}).values())
        for lk in lists_of_kills:
            for i in range(len(lk)):
                lk[i] = ref(kills[tuple(lk[i])])

        return referenced_typedload.load(to_load, cls, namedtuple_to_dataclass=False)
