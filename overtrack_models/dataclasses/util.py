from typing import TYPE_CHECKING, Any, Type

try:
    from typing import Literal
except:
    if TYPE_CHECKING:
        from typing_extensions import Literal
    else:
        class _Literal:
            def __getitem__(self, typeargs: Any) -> Type[str]:
                return str
        Literal = _Literal()


def s2ts(s: float, ms: bool = False, zpad: bool = True, sign: bool = False) -> str:
    prepend = ''
    if s < 0:
        prepend = '-'
        s = -s
    elif sign:
        prepend = '+'

    m = s / 60
    h = m / 60
    if zpad or int(h):
        ts = '%s%02d:%02d:%02d' % (prepend, h, m % 60, s % 60)
    else:
        ts = '%s%02d:%02d' % (prepend, m % 60, s % 60)

    if ms:
        return ts + f'{s % 1 :1.3f}'[1:]
    else:
        return ts
