import itertools
from dataclasses import _MISSING_TYPE, dataclass, fields, is_dataclass, MISSING, Field
from overtrack_models.dataclasses.typedload.typedload import Dumper, Loader
from overtrack_models.dataclasses.typedload.types import Pointer
from typing import Any, Dict, List, Optional, Type, TypeVar

try:
    from typing import ForwardRef
except ImportError:
    from typing import _ForwardRef as ForwardRef

T = TypeVar('T')


def referenced_dump(value: object, ignore_default: bool = True, numpy_support: bool = False) -> Dict[str, Any]:
    return ReferencedDumper(ignore_default=ignore_default, numpy_support=numpy_support).dump(value)


dump = referenced_dump


def referenced_load(value: Any, type_: Type[T], namedtuple_to_dataclass: bool = True) -> T:
    return ReferencedLoader(namedtuple_to_dataclass=namedtuple_to_dataclass).load(value, type_)


load = referenced_load


class ReferencedDumper(Dumper):

    def __init__(self, equality_is_identity: bool = True, **kwargs: Any):
        super().__init__(**kwargs)

        self.pointers: Dict[id, Pointer] = {}

    def _dicthash(self, o) -> int:
        if isinstance(o, dict):
            return hash(frozenset({
                k: self._dicthash(v)
                for k, v in o.items()
            }.items()))
        elif isinstance(o, list):
            return hash(tuple([self._dicthash(e) for e in o]))
        else:
            return hash(o)

    def dump(self, value: Any, *args) -> Any:
        outer_result = {}
        queue = [Pointer(value, 'return', outer_result, ['<base>'])]
        dumped: Dict[int, dict] = {}
        ids = itertools.count()
        dumper = Dumper(numpy_support=self.numpy_support)
        dumper._dispatch = self._dispatch
        while queue:
            pointer = queue.pop(0)
            if id(pointer.to) in dumped:
                existing_dump = dumped[id(pointer.to)]
                if '_id' not in existing_dump:
                    existing_dump['_id'] = next(ids)
                pointer.from_[pointer.key] = {'_ref': existing_dump['_id']}
            elif is_dataclass(pointer.to):
                result = {}
                dumped[id(pointer.to)] = result
                for f in fields(pointer.to):
                    val = getattr(pointer.to, f.name)
                    if self.ignore_default:
                        if val == f.default:
                            continue
                        elif f.default_factory != MISSING and val == f.default_factory():
                            continue
                    queue.append(Pointer(val, f.name, result, pointer.trace + [f.name]))
                pointer.from_[pointer.key] = result
            elif self._is_dict(pointer.to):
                result = {}
                dumped[id(pointer.to)] = result
                for k, v in pointer.to.items():
                    queue.append(Pointer(pointer.to[k], k, result, pointer.trace + [k]))
                pointer.from_[pointer.key] = result
            elif self._is_basic_type(pointer.to):
                # have to do strings before sequences
                pointer.from_[pointer.key] = dumper.dump(pointer.to, *args)
            elif self._is_sequence(pointer.to):
                result = [None for _ in pointer.to]
                for i, v in enumerate(pointer.to):
                    queue.append(Pointer(v, i, result, pointer.trace + [i]))
                pointer.from_[pointer.key] = result
            else:
                pointer.from_[pointer.key] = dumper.dump(
                    pointer.to,
                    *args,
                    _stack=[
                        f'.{e}' if isinstance(e, str) else f'[{e}]'
                        for e in pointer.trace
                    ]
                )

        return outer_result['return']


class ReferencedLoader(Loader):
    _dispatch = Loader._dispatch.copy()

    def __init__(self, forward_refs: Dict[str, Type] = None, namedtuple_to_dataclass: bool = True):
        self.referenced: Dict[int, Any] = {}
        self.types: Dict[str, Type] = dict(forward_refs or {})
        self.namedtuple_to_dataclass = namedtuple_to_dataclass
        self._populated = set()

        self.incomplete = {}

    def _populate_types(self, type_: Type) -> None:
        if id(type_) in self._populated:
            return
        self._populated.add(id(type_))

        if isinstance(type_, str):
            return
        elif type(type_) == ForwardRef:
            return
        elif self._check_basic_type(type_) or self._check_any(type_) or self._check_none_type(type_):
            return
        elif is_dataclass(type_):
            if hasattr(type_, '__name__'):
                self.types[type_.__name__] = type_
            for f in fields(type_):
                self._populate_types(f.type)
        elif self._check_dict(type_):
            try:
                k_type, v_type = type_.__args__
            except:
                try:
                    k_type, v_type = type_.args
                except:
                    return
            self._populate_types(k_type)
            self._populate_types(v_type)
        elif self._check_union(type_):
            for t in type_.__args__:
                self._populate_types(t)
        elif self._check_tuple(type_):
            if len(type_.__args__) == 2 and type_.__args__[1] == Ellipsis:
                # varardic tuple
                self._populate_types(type_.__args__[0])
            else:
                for a in type_.__args__:
                    self._populate_types(a)
        elif self._check_list(type_):
            self._populate_types(type_.__args__[0])

    def load(self, value: Any, type_: Type[T], push_key: Optional[str] = None, _stack=[]) -> T:
        if isinstance(type_, str):
            type_ = self.types[type_]
        elif type(type_) == ForwardRef:
            type_ = self.types[type_.__forward_arg__]
        else:
            self._populate_types(type_)

        if is_dataclass(type_) or self._check_dict(type_):
            if self.namedtuple_to_dataclass and isinstance(value, list):
                # NamedTuple converted to dataclass? support it
                value = {
                    f.name: v
                    for f, v in zip(fields(type_), value)
                }

            if isinstance(value, dict):
                if '_id' in value:
                    id_ = value['_id']

                    # Copy the dict so we don't erase _id from the source data
                    value = dict(value)
                    del value['_id']

                    if id_ not in self.referenced:
                        # Create the dataclass/dict and add it to referenced before deserializing its fields
                        # this allows us to build recursive structures without recursing infinitely
                        if is_dataclass(type_):
                            # noinspection PyArgumentList
                            self.referenced[id_] = type_.__new__(type_)
                        else:
                            self.referenced[id_] = {}
                    else:
                        # The dataclass has been referenced before, but is still unpopulated (since it's data is here)
                        pass

                    result = self.referenced[id_]

                    try:
                        if is_dataclass(type_):
                            self._populate_dataclass(result, value, type_)
                        else:
                            self._populate_dict(result, value, type_)
                    except TypeError as e:
                        del self.referenced[id_]
                        raise e

                    return result
                elif '_ref' in value:
                    ref = value['_ref']
                    if ref not in self.referenced:
                        # The dataclass/dict hasn't been loaded yet, but is being referenced
                        # Create the empty dataclass that will be populated later when we find it
                        if is_dataclass(type_):
                            # noinspection PyArgumentList
                            self.referenced[ref] = type_.__new__(type_)
                        else:
                            self.referenced[ref] = {}
                    return self.referenced[ref]

        return super().load(value, type_, push_key, _stack)

    def _populate_dataclass(self, result: Any, value: Dict[str, Any], type_: Type) -> Any:
        # populate (and load) fields with _id fields first in case one field references another e.g. {'a': {'_ref': 1}, 'b': {'_id': 1}}
        fields_ordered: List[Field] = sorted(
            fields(type_),
            key=lambda f: isinstance(value.get(f.name, None), dict) and '_id' in value[f.name],
            reverse=True
        )
        for f in fields_ordered:
            from_data = MISSING
            if 'migration' in f.metadata:
                from_data = f.metadata['migration'](value)
            elif f.name in value:
                from_data = value[f.name]
            if from_data is not MISSING:
                # object.__setattr__ over setattr to bypass frozen check
                object.__setattr__(result, f.name, self.load(from_data, f.type, f'.{f.name}'))
            elif type(f.default) != _MISSING_TYPE:
                object.__setattr__(result, f.name, f.default)
            elif type(f.default_factory) != _MISSING_TYPE:
                object.__setattr__(result, f.name, f.default_factory())
            elif 'fallback' in f.metadata:
                object.__setattr__(result, f.name, f.metadata['fallback'])
            elif 'fallback_factory' in f.metadata:
                object.__setattr__(result, f.name, f.metadata['fallback_factory']())
            else:
                raise TypeError(f'Could not load {type_} - missing non-default {f.name!r} in fields {value.keys()}')
        return result

    def _populate_dict(self, result: Dict, value: Dict[str, Any], type_: Type) -> Any:
        for k in [k for k in value.keys() if k != '_id']:
            result[k] = value[k]
        return result


if __name__ == '__main__':
    def make_foo():
        @dataclass
        class Foo:
            a: int
        return Foo

    Foo = make_foo()


    @dataclass
    class Bar:
        foo1: Foo
        foo2: Foo
        foo3: Foo
        bar: Optional['Bar'] = None
        bars: Optional[List['Bar']] = None

        id: int = 0

        def __repr__(self) -> str:
            return f'Bar(' \
                   f'.id={id(self)}, ' \
                   f'foo1={self.foo1}, ' \
                   f'foo2={self.foo2}, ' \
                   f'foo3={self.foo3}, ' \
                   f'bar={f"Bar(.id={id(self.bar)}" if self.bar else None}, ' \
                   f'bars={[f"Bar(.id={id(bar)}" for bar in self.bars]}' \
                   f')'

        __str__ = __repr__

    foo = Foo(1)
    bar = Bar(
        foo,
        foo,
        Foo(1)
    )
    bar.bar = bar

    bar2 = Bar(foo, Foo(2), Foo(2), bar, [bar])
    bar2.bars.append(bar2)
    bar.bars = [bar2, bar2, bar]

    # print(bar)
    # print(bar.bars[1])

    bardata = referenced_dump(bar)
    print(bardata)
    #
    # dbar = referenced_load(bardata, Bar)
    # print(dbar)
    # print(dbar.bars[1])

