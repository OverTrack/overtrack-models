from dataclasses import dataclass, field
from typing import Optional, Dict, Tuple, List

from overtrack_models.dataclasses import typedload
from overtrack_models.dataclasses.valorant import MapName, AgentName


@dataclass
class Winrate:
    wins: int = 0
    total: int = 0

    @property
    def winrate(self) -> Optional[float]:
        if not self.total:
            return None
        return self.wins / self.total

    def __str__(self):
        return f'{self.__class__.__name__}(' \
               f'wins={self.wins}, ' \
               f'total={self.total}, ' \
               f'winrate={round(self.winrate*100, 2) if self.total else "..."}%' \
               f')'
    __repr__ = __str__


@dataclass
class Winrates:
    games: Winrate = field(default_factory=Winrate)
    rounds: Winrate = field(default_factory=Winrate)
    attacking_rounds: Winrate = field(default_factory=Winrate)
    defending_rounds: Winrate = field(default_factory=Winrate)


MapsAgentsDict = Dict[Tuple[Optional[MapName], Optional[AgentName]], Winrates]


@dataclass
class MapAgentWinrates:
    maps_agents: MapsAgentsDict

    def map(self, m: MapName) -> Winrates:
        return self.maps_agents[(m, None)]

    def agent(self, a: AgentName) -> Winrates:
        return self.maps_agents[(None, a)]

    def map_agent(self, m: MapName, a: AgentName) -> Winrates:
        return self.maps_agents[(m, a)]

    def all(self) -> Winrates:
        return self.maps_agents[(None, None)]

    def to_dict(self) -> Dict:
        return {
            'maps_agents': typedload.dump(list(self.maps_agents.items()))
        }

    @classmethod
    def from_dict(cls, data: Dict):
        return cls(typedload.load({
            tuple(k): v
            for (k, v)
            in data['maps_agents']
        }, MapsAgentsDict))
