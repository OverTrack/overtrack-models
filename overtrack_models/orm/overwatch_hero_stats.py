import datetime

from pynamodb.attributes import NumberAttribute, UnicodeAttribute, JSONAttribute, BooleanAttribute
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection

from overtrack_models.orm.common import OverTrackModel, GlobalSecondaryIndexExt, make_gsi


class OverwatchHeroStats(OverTrackModel):
    class Meta:
        table_name = 'overtrack-hero-stats-2'
        region = 'us-west-2'

    user_id = NumberAttribute(hash_key=True)
    timestamp_hero = UnicodeAttribute(range_key=True)

    timestamp = NumberAttribute()
    season = NumberAttribute()

    hero = UnicodeAttribute()
    account = UnicodeAttribute()
    role = UnicodeAttribute(null=True)

    sr = NumberAttribute(null=True)
    rank = UnicodeAttribute(null=True)

    game_key = UnicodeAttribute()
    game_result = UnicodeAttribute()
    competitive = BooleanAttribute()
    custom_game = BooleanAttribute()
    map_name = UnicodeAttribute()
    map_type = UnicodeAttribute()

    time_played = NumberAttribute()
    from_endgame = BooleanAttribute()

    eliminations = NumberAttribute()
    objective_kills = NumberAttribute()
    objective_time = NumberAttribute()
    hero_damage_done = NumberAttribute()
    healing_done = NumberAttribute()
    deaths = NumberAttribute()
    final_blows = NumberAttribute()

    hero_specific_stats = JSONAttribute(null=True)

    user_id_timestamp_index: GlobalSecondaryIndexExt['OverwatchHeroStats'] = make_gsi(user_id=user_id, timestamp=timestamp)
    hero_timestamp_index: GlobalSecondaryIndexExt['OverwatchHeroStats'] = make_gsi(hero=hero, timestamp=timestamp)

    def __init__(self, user_id: int, timestamp: float, hero: str, **kwargs):
        kwargs['timestamp_hero'] = (
            datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d-%H-%M-%S') +
            '/' +
            hero
        )
        super().__init__(user_id=user_id, timestamp=timestamp, hero=hero, **kwargs)
