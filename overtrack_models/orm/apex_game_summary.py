import datetime
import os
from typing import Any, Optional

from pynamodb.attributes import ListAttribute, MapAttribute, NumberAttribute, UnicodeAttribute

from overtrack_models.orm.common import OverTrackModel, TupleAttribute, make_gsi, GlobalSecondaryIndexExt


class Weapon(MapAttribute):
    name = UnicodeAttribute()
    knockdowns = NumberAttribute()
    time_held = NumberAttribute()


class Ring(MapAttribute):
    round = NumberAttribute()
    center = TupleAttribute(2, null=True)

    def __str__(self) -> str:
        return f'Ring({self.round}, center={self.center})'

    __repr__ = __str__


class Rank(MapAttribute):
    rank = UnicodeAttribute()
    tier = UnicodeAttribute(null=True)
    rp = NumberAttribute(null=True)
    rp_change = NumberAttribute(null=True)

    def __str__(self) -> str:
        return OverTrackModel.__str__(self)

    __repr__ = __str__


class ApexGameSummary(OverTrackModel):
    class Meta:
        table_name = os.environ.get('APEX_GAME_TABLE', 'overtrack_apex_games')
        region = os.environ.get('DYNAMODB_REGION', 'us-west-2')

    key = UnicodeAttribute(hash_key=True)
    user_id = NumberAttribute()
    source = UnicodeAttribute(null=True)
    frames_uri = UnicodeAttribute(null=True)

    season = NumberAttribute(default=0)

    timestamp = NumberAttribute()
    duration = NumberAttribute()
    champion = UnicodeAttribute(null=True)
    squadmates = TupleAttribute(2)
    squadmate_names = TupleAttribute(2)
    placed = NumberAttribute()
    kills = NumberAttribute()
    knockdowns = NumberAttribute(null=True)
    squad_kills = NumberAttribute(null=True)

    landed = UnicodeAttribute(null=True)
    drop_coordinates = TupleAttribute(2, null=True)
    rings = ListAttribute(of=Ring)
    weapons = ListAttribute(of=Weapon, null=True)
    rank = Rank(null=True)

    url = UnicodeAttribute(null=True)

    player_name = UnicodeAttribute(null=True)
    match_id = UnicodeAttribute(null=True)
    scrims = UnicodeAttribute(null=True)

    user_id_time_index: GlobalSecondaryIndexExt['ApexGameSummary'] = make_gsi('user_id_time_index', user_id=user_id, timestamp=timestamp)
    season_timestamp_index: GlobalSecondaryIndexExt['ApexGameSummary'] = make_gsi(season=season, timestamp=timestamp)
    match_id_index: GlobalSecondaryIndexExt['ApexGameSummary'] = make_gsi(match_id=match_id)
    scrims_match_id_index: GlobalSecondaryIndexExt['ApexGameSummary'] = make_gsi(scrims=scrims, match_id=match_id)

    @classmethod
    def create(
            cls,
            game: Any,
            user_id: int,
            url: Optional[str] = None,
            frames_uri: Optional[str] = None,
            source: Optional[str] = None) -> 'ApexGameSummary':
        if game.rank:
            rank = Rank(rank=game.rank.rank, tier=game.rank.rank_tier, rp=game.rank.rp, rp_change=game.rank.rp_change)
        else:
            rank = None

        season_id = game.season
        if game.scrims:
            # TODO: better handling of scrim IDs
            season_id = 2000
        elif game.solo or game.duos:
            season_id += 1000

        return cls(
            key=game.key,
            user_id=user_id,
            player_name=game.squad.player.name,
            source=source,
            frames_uri=frames_uri,

            season=season_id,

            timestamp=game.timestamp,
            duration=game.duration,
            champion=game.squad.player.champion,
            squadmates=(
                game.squad.squadmates[0].champion if game.squad.squadmates[0] else None,
                game.squad.squadmates[1].champion if game.squad.squadmates[1] else None
            ),
            squadmate_names=(
                game.squad.squadmates[0].name if game.squad.squadmates[0] else None,
                game.squad.squadmates[1].name if game.squad.squadmates[1] else None
            ),
            placed=game.placed,
            kills=game.kills,
            knockdowns=len(game.combat.knockdowns),
            squad_kills=game.squad.squad_kills,

            landed=game.route.landed_name,
            drop_coordinates=game.route.landed_location,
            rings=[],  # [Ring(round=i + 1, center=r.center if r else None) for i, r in enumerate(game.route.rings)] if game.route.rings else None,
            weapons=[
                Weapon(name=w.weapon, knockdowns=w.knockdowns, time_held=w.time_held) for w in game.weapons.weapon_stats
            ],
            rank=rank,

            url=url,

            scrims=game.scrims,
            match_id=game.match_id,
        )

    @property
    def time(self) -> datetime.datetime:
        return datetime.datetime.utcfromtimestamp(self.timestamp)

    def __str__(self) -> str:
        return f'ApexGameSummary(key={self.key}, season={self.season}, time={self.time}, placed={self.placed}, url={self.url}, rank={self.rank}, rings={self.rings})'

    __repr__ = __str__
