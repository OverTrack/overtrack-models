import os
from typing import Dict, Optional

from pynamodb.attributes import BooleanAttribute, JSONAttribute, NumberAttribute, UnicodeAttribute

from overtrack_models.orm.common import GlobalSecondaryIndexExt, OverTrackModel, make_gsi


class DiscordBotNotification(OverTrackModel):
    class Meta:
        table_name = os.environ.get('DISCORD_BOT_NOTIFICATION_TABLE', 'overtrack_discord_bot_notification')
        region = os.environ.get('DYNAMODB_REGION', 'us-west-2')

    key = UnicodeAttribute(hash_key=True, null=False)
    user_id = NumberAttribute(null=False)
    discord_user_id = UnicodeAttribute(null=True)
    announce_message_id = UnicodeAttribute(null=True)

    game = UnicodeAttribute()

    guild_id = UnicodeAttribute(null=False)
    guild_name = UnicodeAttribute(null=False)

    channel_name = UnicodeAttribute(null=False)

    notification_data = JSONAttribute(null=False)

    is_parent = BooleanAttribute(default=True)
    autoapprove_children = BooleanAttribute(default=False)
    parent_key = UnicodeAttribute(null=True, default=None)

    user_id_index: GlobalSecondaryIndexExt['DiscordBotNotification'] = make_gsi('user_id_index', user_id=user_id)
    guild_id_index: GlobalSecondaryIndexExt['DiscordBotNotification'] = make_gsi(guild_id=guild_id)

    @classmethod
    def create(
        cls,
        user_id: int,
        discord_user_id: str,
        announce_message_id: str,
        game: str,
        channel_id: str,
        guild_id: str,
        guild_name: str,
        channel_name: str,
        notification_data: Dict[str, object],
        is_parent: bool = True,
        autoapprove_children: bool = False,
        parent_key: Optional[str] = None,
    ):
        return DiscordBotNotification(
            key=f'{user_id}.{game}.{channel_id}',
            user_id=user_id,
            discord_user_id=discord_user_id,
            announce_message_id=announce_message_id,

            game=game,
            guild_id=guild_id,
            guild_name=guild_name,
            channel_name=channel_name,

            notification_data=notification_data,

            is_parent=is_parent,
            autoapprove_children=autoapprove_children,
            parent_key=parent_key,
        )

    @property
    def channel_id(self) -> str:
        return self.key.split('.')[2]


class TwitchBotNotification(OverTrackModel):
    class Meta:
        table_name = os.environ.get('TWITCH_BOT_NOTIFICATION_TABLE', 'overtrack_twitch_bot_notification')
        region = os.environ.get('DYNAMODB_REGION', 'us-west-2')

    key = UnicodeAttribute(hash_key=True, null=False)
    user_id = NumberAttribute(null=False)
    game = UnicodeAttribute()

    twitch_user_id = NumberAttribute()
    twitch_channel_name = UnicodeAttribute()

    notification_data = JSONAttribute(null=True)

    user_id_index: GlobalSecondaryIndexExt['TwitchBotNotification'] = make_gsi('user_id_index', user_id=user_id)

    @classmethod
    def create(cls,
               user_id: int,
               game: str,
               twitch_user_id: int,
               channel_name: str,
               notification_data: Optional[Dict[str, object]] = None):
        return TwitchBotNotification(
            key=f'{user_id}.{game}.{twitch_user_id}',
            user_id=user_id,
            game=game,
            twitch_user_id=twitch_user_id,
            twitch_channel_name=channel_name,
            notification_data=notification_data
        )
